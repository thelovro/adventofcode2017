﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day14
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"flqrgnkx", "8108");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<string, int[]> field = PrepareData(input);

            return field.Select(x => x.Value.Sum()).Sum().ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<string, int[]> field = PrepareData(input);

            //we prepare aray of 128x128
            //and we replace all 1 with -1 - so we can match groups more easily
            int[,] array = PrepareArray(field);

            //find first -1
            int groupCounter = 1;
            for (int i = 0; i < 128; i++)
            {
                for (int j = 0; j < 128; j++)
                {
                    if (array[i, j] == -1)
                    {
                        array = SetGroup(array, i, j, groupCounter++);
                    }
                }
            }

            return (groupCounter - 1).ToString();
        }

        private int[,] SetGroup(int[,] array, int i, int j, int groupCounter)
        {
            array[i, j] = groupCounter;
            //up
            if (j > 0 && array[i, j - 1] == -1)
            {
                array = SetGroup(array, i, j - 1, groupCounter);
            }
            //right
            if (i < 128 - 1 && array[i + 1, j] == -1)
            {
                array = SetGroup(array, i + 1, j, groupCounter);
            }
            //down
            if (j < 128 - 1 && array[i, j + 1] == -1)
            {
                array = SetGroup(array, i, j + 1, groupCounter);
            }
            //left
            if (i > 0 && array[i - 1, j] == -1)
            {
                array = SetGroup(array, i - 1, j, groupCounter);
            }

            return array;
        }

        private int[,] PrepareArray(Dictionary<string, int[]> field)
        {
            int[,] array = new int[128, 128];
            int counter = 0;
            foreach (var list in field.Values)
            {
                for (int i = 0; i < list.Length; i++)
                {
                    array[counter, i] = list[i] *= -1;
                }
                counter++;
            }

            return array;
        }

        private Dictionary<string, int[]> PrepareData(string[] input)
        {
            string code = input[0];
            Dictionary<string, int[]> field = new Dictionary<string, int[]>();
            for (int i = 0; i < 128; i++)
            {
                string key = string.Format("{0}-{1}", code, i);
                field.Add(key, CalculateHashKnotAndConvertToBytes(key));
            }

            return field;
        }

        private int[] CalculateHashKnotAndConvertToBytes(string input)
        {
            string hexaDecimal = Day10.Program.GetAsciiCharactersKnotHash(input);
            string bits = "";
            foreach (char c in hexaDecimal)
            {
                bits += HexToBin(c.ToString());
            }
            return bits.Select(x => int.Parse(x.ToString())).ToArray();
        }

        private static string HexToBin(string value)
        {
            return Convert.ToString(Convert.ToInt32(value, 16), 2).PadLeft(value.Length * 4, '0');
        }
    }
}