﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day15
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"Generator A starts with 65
Generator B starts with 8921", "588");

            program.SampleInputPartTwo.Add(@"Generator A starts with 65
Generator B starts with 8921", "309");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int startA = int.Parse(input[0].Replace("Generator A starts with ", ""));
            int startB = int.Parse(input[1].Replace("Generator B starts with ", ""));
            int factorA = 16807;
            int factorB = 48271;
            int divideBy = 2147483647;

            long solutionA = startA;
            long solutionB = startB;

            int counter = 0;
            for (int i = 0; i < 40 * 1000 * 1000; i++)
            {
                solutionA = (solutionA * factorA) % divideBy;
                solutionB = (solutionB * factorB) % divideBy;
                var bitsA = Convert.ToString(solutionA, 2).PadLeft(16, '0');
                var bitsB = Convert.ToString(solutionB, 2).PadLeft(16, '0');

                if (bitsA.Substring(bitsA.Length - 16) == bitsB.Substring(bitsB.Length - 16))
                {
                    counter++;
                }
            }

            return counter.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            int startA = int.Parse(input[0].Replace("Generator A starts with ", ""));
            int startB = int.Parse(input[1].Replace("Generator B starts with ", ""));
            int factorA = 16807;
            int factorB = 48271;
            int divideBy = 2147483647;

            long solutionA = startA;
            long solutionB = startB;

            int counter = 0;
            int counterJ = 0;
            int counterPairs = 0;
            for (int i = 0; i < 45 * 1000 * 1000; i++)
            {
                if (counterPairs == 5 * 1000 * 1000)
                {
                    break;
                }

                solutionA = (solutionA * factorA) % divideBy;
                if (solutionA % 4 != 0)
                {
                    continue;
                }

                for (int j = counterJ; j < 45 * 1000 * 1000; j++)
                {
                    solutionB = (solutionB * factorB) % divideBy;
                    if (solutionB % 8 != 0)
                    {
                        continue;
                    }
                    counterPairs++;
                    counterJ = j + 1;

                    var bitsA = Convert.ToString(solutionA, 2).PadLeft(16, '0');
                    var bitsB = Convert.ToString(solutionB, 2).PadLeft(16, '0');

                    if (bitsA.Substring(bitsA.Length - 16) == bitsB.Substring(bitsB.Length - 16))
                    {
                        counter++;
                    }

                    break;
                }
            }

            return counter.ToString();
        }
    }
}