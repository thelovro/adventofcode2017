﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day04
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"aa bb cc dd ee
aa bb cc dd aa
aa bb cc dd aaa", "2");

            program.SampleInputPartTwo.Add(@"abcde fghij
abcde xyz ecdab
a ab abc abd abf abj
iiii oiii ooii oooi oooo
oiii ioii iioi iiio", "3");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int sum = 0;
            foreach (var row in input)
            {
                var values = row.Split(' ', StringSplitOptions.RemoveEmptyEntries);

                if (values.Count() == values.Distinct().Count())
                {
                    sum++;
                }
            }

            return sum.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            int sum = 0;
            foreach (var row in input)
            {
                var values = row.Split(' ', StringSplitOptions.RemoveEmptyEntries);

                List<string> x = new List<string>(values.Select(a => Sort(a)));
                if (x.Count == x.Distinct().Count())
                {
                    sum++;
                }
            }

            return sum.ToString();
        }

        public static string Sort(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Sort(charArray);
            return new string(charArray);
        }
    }
}