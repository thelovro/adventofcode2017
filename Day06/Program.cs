﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day06
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"0	2	7	0", "5");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            var values = input[0].Split("\t").Select(a => int.Parse(a)).ToArray();

            List<string> listOfCombinations = new List<string>();
            listOfCombinations.Add(string.Join("", values.ToArray()));

            int counter = 0;
            string lastCombination = "";
            while (!listOfCombinations.Contains(lastCombination))
            {
                listOfCombinations.Add(lastCombination);

                int maxValue = values.Max();
                int positionWithMaxValue = Array.IndexOf(values, maxValue);

                int numberOfSteps = values[positionWithMaxValue];
                values[positionWithMaxValue] = 0;

                int position = positionWithMaxValue;
                while (numberOfSteps > 0)
                {
                    position++;
                    if (position == values.Count())
                    {
                        position = 0;
                    }

                    values[position]++;
                    numberOfSteps--;
                }

                lastCombination = string.Join("", values.ToArray());
                counter++;
            }

            return counter.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            var values = input[0].Split("\t").Select(a => int.Parse(a)).ToArray();

            Dictionary<int, string> listOfCombinations = new Dictionary<int, string>();

            int counter = 0;
            string lastCombination = string.Join("", values.ToArray());
            while (!listOfCombinations.Any(x => x.Value == lastCombination))
            {
                listOfCombinations.Add(counter, lastCombination);

                int maxValue = values.Max();
                int positionWithMaxValue = Array.IndexOf(values, maxValue);

                int numberOfSteps = values[positionWithMaxValue];
                values[positionWithMaxValue] = 0;

                int position = positionWithMaxValue;
                while (numberOfSteps > 0)
                {
                    position++;
                    if (position == values.Count())
                    {
                        position = 0;
                    }

                    values[position]++;
                    numberOfSteps--;
                }

                lastCombination = string.Join("", values.ToArray());
                counter++;
            }

            int cycleSize = counter - listOfCombinations.First(x => x.Value == lastCombination).Key;

            return cycleSize.ToString();
        }
    }
}