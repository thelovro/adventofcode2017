﻿using Common;
using System;
using System.Collections.Generic;

namespace Day23
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Operation> operations = ParseOperations(input);
            Dictionary<char, long> registers = InitRegisters();

            long counter = RunCode(operations, registers, false);

            return counter.ToString();
        }

        private long RunCode(List<Operation> operations, Dictionary<char, long> registers, bool skipOperations)
        {
            long counter = 0;
            int index = 0;
            while (true)
            {
                long y = long.TryParse(operations[index].Value, out _)
                        ? Convert.ToInt64(operations[index].Value)
                        : Convert.ToInt64(registers[operations[index].Value[0]]);

                if (operations[index].Instruction == "set")
                {
                    registers[operations[index].Register] = y;
                    index++;
                }
                else if (operations[index].Instruction == "sub")
                {

                    if (skipOperations
                        && y == -1
                        && operations[index].Register != 'h'
                        && (operations[index].Register != 'd' || registers['f'] == 0))
                    {
                        //loop found => set it to value of register b (because JNZ is compared by value in register b
                        registers[operations[index].Register] = Convert.ToInt64(registers['b']);
                        if (Convert.ToInt64(registers['b']) % Convert.ToInt64(registers['d']) == 0)
                        {
                            registers['f'] = 0;
                        }
                    }
                    else if (skipOperations && operations[index].Register == 'd')
                    {
                        //increase register d until b is dividable by d
                        while(Convert.ToInt64(registers['b']) % Convert.ToInt64(registers['d']) > 0)
                        {
                            registers['d']++;
                        }
                    }
                    else
                    {
                        registers[operations[index].Register] -= y;
                    }

                    index++;
                }
                else if (operations[index].Instruction == "mul")
                {
                    registers[operations[index].Register] *= y;
                    counter++;
                    index++;
                }
                else if (operations[index].Instruction == "jnz")
                {
                    if (operations[index].Register == '1' || registers[operations[index].Register] != 0)
                    {
                        index += (int)y;
                    }
                    else
                    {
                        index++;
                    }
                }

                if (index < 0 || index >= operations.Count)
                {
                    break;
                }
            }

            return counter;
        }

        private List<Operation> ParseOperations(string[] input)
        {
            List<Operation> operations = new();
            foreach(var line in input)
            {
                string[] parts = line.Split(' ');
                operations.Add(new Operation(parts[0], parts[1][0], parts[2]));
            }

            return operations;
        }

        private Dictionary<char, long> InitRegisters()
        {
            Dictionary<char, long> registers = new();
            for (int i = 97; i <= 104; i++)
            {
                registers.Add((char)i, 0);
            }

            return registers;
        }

        public record Operation(string Instruction, char Register, string Value);

        protected override string FindSecondSolution(string[] input)
        {
            List<Operation> operations = ParseOperations(input);
            Dictionary<char, long> registers = InitRegisters();

            registers['a'] = 1;
            RunCode(operations, registers, true);

            return registers['h'].ToString();
        }
    }
}
