﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day24
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"0/2
2/2
2/3
3/4
3/5
0/1
10/1
9/10", "31");

            program.SampleInputPartTwo.Add(@"0/2
2/2
2/3
3/4
3/5
0/1
10/1
9/10", "19");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            //parse input
            Dictionary<int,List<int>> componentsCatalogue = GetComponents(input);

            //init the queue
            Queue<Component> openList = new Queue<Component>();
            int freeConnector = 0;
            foreach (var catalogueItem in componentsCatalogue.Where(c => c.Value.Contains(freeConnector)))
            {
                openList.Enqueue(new Component()
                {
                    UsedComponents = new List<int>() { catalogueItem.Key },
                    Value = catalogueItem.Value.Sum(),
                    OpenConnector = catalogueItem.Value.Except(new List<int> { freeConnector }).Count() == 0 ? freeConnector : catalogueItem.Value.Except(new List<int> { freeConnector }).First()
                });
            }

            //find strongest bridge
            int maxSum = 0;
            while (openList.Count > 0)
            {
                var item = openList.Dequeue();

                if (item.Value > maxSum)
                {
                    maxSum = item.Value;
                }

                foreach (var catalogueItem in componentsCatalogue.Where(c => !item.UsedComponents.Contains(c.Key)).Where(c => c.Value.Contains(item.OpenConnector)))
                {
                    var newComponent = new Component
                    {
                        UsedComponents = new List<int>() { catalogueItem.Key },
                        Value = item.Value + catalogueItem.Value.Sum(),
                        OpenConnector = catalogueItem.Value.Except(new List<int> { item.OpenConnector }).Count() == 0 ? item.OpenConnector : catalogueItem.Value.Except(new List<int> { item.OpenConnector }).First()
                    };

                    newComponent.UsedComponents.AddRange(item.UsedComponents);

                    openList.Enqueue(newComponent);
                }
            }

            return maxSum.ToString();
        }

        private Dictionary<int, List<int>> GetComponents(string[] input)
        {
            Dictionary<int, List<int>> components = new();
            for (int i = 0; i < input.Length; i++)
            {
                string line = input[i];
                components.Add(i, new List<int>() { Convert.ToInt32(line.Split('/')[0]), Convert.ToInt32(line.Split('/')[1]) });
            }

            return components;
        }


        public class Component
        {
            public List<int> UsedComponents { get; set; }
            public int Value { get; set; }

            public int Length { get; set; }
            public int OpenConnector { get; set; }
        }
        

        protected override string FindSecondSolution(string[] input)
        {
            //parse input
            Dictionary<int, List<int>> componentsCatalogue = GetComponents(input);

            //init the queue
            Queue<Component> openList = new Queue<Component>();
            int freeConnector = 0;
            foreach (var catalogueItem in componentsCatalogue.Where(c => c.Value.Contains(freeConnector)))
            {
                openList.Enqueue(new Component()
                {
                    UsedComponents = new List<int>() { catalogueItem.Key },
                    Value = catalogueItem.Value.Sum(),
                    Length = 1,
                    OpenConnector = catalogueItem.Value.Except(new List<int> { freeConnector }).Count() == 0 ? freeConnector : catalogueItem.Value.Except(new List<int> { freeConnector }).First()
                });
            }

            //find longest strongest bridge
            int maxLength = 0;
            int maxSum = 0;
            while (openList.Count > 0)
            {
                var item = openList.Dequeue();

                if (item.Length > maxLength)
                {
                    maxSum = 0;
                    maxLength = item.Length;
                }
                if (item.Length == maxLength)
                {
                    if (item.Value > maxSum)
                    {
                        maxSum = item.Value;
                    }
                }

                foreach (var catalogueItem in componentsCatalogue.Where(c => !item.UsedComponents.Contains(c.Key)).Where(c => c.Value.Contains(item.OpenConnector)))
                {
                    var newComponent = new Component
                    {
                        UsedComponents = new List<int>() { catalogueItem.Key },
                        Value = item.Value + catalogueItem.Value.Sum(),
                        Length = item.Length + 1,
                        OpenConnector = catalogueItem.Value.Except(new List<int> { item.OpenConnector }).Count() == 0 ? item.OpenConnector : catalogueItem.Value.Except(new List<int> { item.OpenConnector }).First()
                    };

                    newComponent.UsedComponents.AddRange(item.UsedComponents);

                    openList.Enqueue(newComponent);
                }
            }

            return maxSum.ToString();
        }
    }
}
