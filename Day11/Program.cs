﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day11
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"ne,ne,ne", "3");
            program.SampleInputPartOne.Add(@"ne,ne,sw,sw", "0");
            program.SampleInputPartOne.Add(@"ne,ne,s,s", "2");
            program.SampleInputPartOne.Add(@"se,sw,se,sw,sw", "3");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int solution = CalculateNumberOfStepsInternal(input[0].Split(","));

            return solution.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            int maxSteps = 0;
            for (int i = 0; i < input[0].Split(",").Length; i++)
            {
                int currentSteps = CalculateNumberOfStepsInternal(input[0].Split(",").Take(i).ToArray());
                if (currentSteps > maxSteps)
                {
                    maxSteps = currentSteps;
                }
            }

            return maxSteps.ToString();
        }

        private static int CalculateNumberOfStepsInternal(string[] input)
        {
            var directions = input.GroupBy(x => x).Select(x => new { Direction = x.Key, Counter = x.Count() }).ToDictionary(x => x.Direction, x => x.Counter);

            int directionSouth = directions.Keys.Contains("s") ? directions["s"] : 0;
            int directionNorth = directions.Keys.Contains("n") ? directions["n"] : 0;
            int directionNorthEast = directions.Keys.Contains("ne") ? directions["ne"] : 0;
            int directionNorthWest = directions.Keys.Contains("nw") ? directions["nw"] : 0;
            int directionSouthEast = directions.Keys.Contains("se") ? directions["se"] : 0;
            int directionSouthWest = directions.Keys.Contains("sw") ? directions["sw"] : 0;

            int dNS = directionNorth - directionSouth;
            int dNESW = directionNorthEast - directionSouthWest; // > 0 -> we go NE
            int dNWSE = directionNorthWest - directionSouthEast; // > 0 -> we go NW

            while (dNESW > 0 && dNWSE > 0)
            {
                dNESW--;
                dNWSE--;
                dNS++;
            }
            while (dNESW < 0 && dNWSE < 0)
            {
                dNESW++;
                dNWSE++;
                dNS--;
            }

            if (dNS >= 0 && dNESW >= 0 && dNWSE >= 0 || dNS <= 0 && dNESW <= 0 && dNWSE <= 0)
            {
                return Math.Abs(dNS) + Math.Abs(dNESW) + Math.Abs(dNWSE);
            }

            return Math.Max(Math.Max(Math.Abs(dNS), Math.Abs(dNESW)), Math.Abs(dNWSE));
        }
    }
}