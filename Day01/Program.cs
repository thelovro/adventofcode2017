﻿using Common;
using System;

namespace Day01
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"1122", "3");
            program.SampleInputPartOne.Add(@"1111", "4");
            program.SampleInputPartOne.Add(@"1234", "0");
            program.SampleInputPartOne.Add(@"91212129", "9");

            program.SampleInputPartTwo.Add(@"1212", "6");
            program.SampleInputPartTwo.Add(@"1221", "0");
            program.SampleInputPartTwo.Add(@"123425", "4");
            program.SampleInputPartTwo.Add(@"123123", "12");
            program.SampleInputPartTwo.Add(@"12131415", "4");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            var sequence = input[0];
            int sum = 0;

            for (int i = 0; i < sequence.Length; i++)
            {
                if (i < sequence.Length - 1)
                {
                    if (sequence[i] == sequence[i + 1])
                    {
                        sum += int.Parse(sequence[i].ToString());
                    }
                }
                else
                {
                    //smo na koncu seznama
                    if (sequence[i] == sequence[0])
                    {
                        sum += int.Parse(sequence[i].ToString());
                    }
                }
            }

            return sum.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            var sequence = input[0];
            int halfLength = sequence.Length / 2;
            int sum = 0;

            for (int i = 0; i < halfLength; i++)
            {
                if (sequence[i] == sequence[i + halfLength])
                {
                    sum += int.Parse(sequence[i].ToString());
                }
            }

            return (sum * 2).ToString();
        }
    }
}