﻿using Common;
using System;
using System.Linq;

namespace Day10
{
    public class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"3,4,1,5", "12");

            program.SampleInputPartTwo.Add(@" ", "a2582a3a0e66e6e86e3812dcb672a272".ToUpper());
            program.SampleInputPartTwo.Add(@"AoC 2017", "33efeb34ea91902bb2f59c9920caa6cd".ToUpper());
            program.SampleInputPartTwo.Add(@"1,2,3", "3efbe78a8d82f29979031a4aa0b16a9d".ToUpper());
            program.SampleInputPartTwo.Add(@"1,2,4", "63960835bcdc130f0b66d7ff4f6a5a8e".ToUpper());

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            string sequence = input[0];
            int listSize = IsTest ? 5 : 256;
            int[] listOfNumbers = new int[listSize];
            for (int i = 0; i < listSize; i++)
            {
                listOfNumbers[i] = i;
            }

            ReorderList(listOfNumbers, sequence.Split(","));

            return (listOfNumbers[0] * listOfNumbers[1]).ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            string sequence = input[0].Trim();
            string solution = GetAsciiCharactersKnotHash(sequence);

            return solution;
        }

        public static string GetAsciiCharactersKnotHash(string sequence)
        {
            byte[] asciiBytesTmp = System.Text.Encoding.ASCII.GetBytes(sequence);
            byte[] asciiBytes = new byte[asciiBytesTmp.Length + 5];
            asciiBytesTmp.CopyTo(asciiBytes, 0);
            asciiBytes[asciiBytesTmp.Length + 0] = 17;
            asciiBytes[asciiBytesTmp.Length + 1] = 31;
            asciiBytes[asciiBytesTmp.Length + 2] = 73;
            asciiBytes[asciiBytesTmp.Length + 3] = 47;
            asciiBytes[asciiBytesTmp.Length + 4] = 23;

            //reset list of numbers
            int[] listOfNumbers = new int[256];
            for (int i = 0; i < 256; i++)
            {
                listOfNumbers[i] = i;
            }

            ReorderListPartTwo(ref listOfNumbers, asciiBytes);
            int[] denseHash = CalculateDenseHash(listOfNumbers);
            string hex = GetHex(denseHash);

            return hex;
        }

        private static void ReorderList(int[] listOfNumbers, string[] input)
        {
            int position = 0;
            int skipSize = 0;

            foreach (string number in input)
            {
                int length = int.Parse(number);
                var sublist = listOfNumbers.AsEnumerable().Concat(listOfNumbers).Skip(position).Take(length).Reverse().ToArray();

                for (int i = 0; i < sublist.Count(); i++)
                {
                    int newPosition = position + i;
                    if (newPosition >= listOfNumbers.Length)
                    {
                        newPosition = newPosition - listOfNumbers.Length;
                    }

                    listOfNumbers[newPosition] = sublist[i];
                }

                int increment = length + skipSize++;

                position += increment;
                if (position >= listOfNumbers.Length)
                {
                    position = position - listOfNumbers.Length;
                }
            }
        }

        private static void ReorderListPartTwo(ref int[] listOfNumbers, byte[] input)
        {
            int position = 0;
            int skipSize = 0;

            for (int x = 0; x < 64; x++)
            {
                foreach (byte part in input)
                {
                    string number = part.ToString();

                    int length = int.Parse(number);
                    var sublist = listOfNumbers.AsEnumerable().Concat(listOfNumbers).Skip(position).Take(length).Reverse().ToArray();

                    for (int i = 0; i < sublist.Count(); i++)
                    {
                        int newPosition = position + i;
                        while (newPosition >= listOfNumbers.Length)
                        {
                            newPosition = newPosition - listOfNumbers.Length;
                        }

                        listOfNumbers[newPosition] = sublist[i];
                    }

                    int increment = length + skipSize++;

                    position += increment;
                    while (position >= listOfNumbers.Length)
                    {
                        position = position - listOfNumbers.Length;
                    }
                }
            }
        }

        private static int[] CalculateDenseHash(int[] listOfNumbers)
        {
            int[] denseHash = new int[16];
            for (int i = 0; i < 16; i++)
            {
                denseHash[i] = listOfNumbers[i * 16 + 0] ^ listOfNumbers[i * 16 + 1] ^ listOfNumbers[i * 16 + 2] ^ listOfNumbers[i * 16 + 3] ^
                                listOfNumbers[i * 16 + 4] ^ listOfNumbers[i * 16 + 5] ^ listOfNumbers[i * 16 + 6] ^ listOfNumbers[i * 16 + 7] ^
                                listOfNumbers[i * 16 + 8] ^ listOfNumbers[i * 16 + 9] ^ listOfNumbers[i * 16 + 10] ^ listOfNumbers[i * 16 + 11] ^
                                listOfNumbers[i * 16 + 12] ^ listOfNumbers[i * 16 + 13] ^ listOfNumbers[i * 16 + 14] ^ listOfNumbers[i * 16 + 15];
            }

            return denseHash;
        }

        private static string GetHex(int[] denseHash)
        {
            string hex = string.Empty;
            foreach (int part in denseHash)
            {
                hex += part.ToString("X2");
            }

            return hex;
        }
    }
}