﻿using Common;
using System;
using System.Collections.Generic;

namespace Day09
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"{}", "1");
            program.SampleInputPartOne.Add(@"{{{}}}", "6");
            program.SampleInputPartOne.Add(@"{{},{}}", "5");
            program.SampleInputPartOne.Add(@"{{{},{},{{}}}}", "16");
            program.SampleInputPartOne.Add(@"{<a>,<a>,<a>,<a>}", "1");
            program.SampleInputPartOne.Add(@"{{<ab>},{<ab>},{<ab>},{<ab>}}", "9");
            program.SampleInputPartOne.Add(@"{{<!!>},{<!!>},{<!!>},{<!!>}}", "9");
            program.SampleInputPartOne.Add(@"{{<a!>},{<a!>},{<a!>},{<ab>}}", "3");

            program.SampleInputPartTwo.Add(@"<>", "0");

            program.SampleInputPartTwo.Add(@"<random characters>", "17");
            program.SampleInputPartTwo.Add(@"<<<<>", "3");
            program.SampleInputPartTwo.Add(@"<{!>}>", "2");
            program.SampleInputPartTwo.Add(@"<!!>", "0");
            program.SampleInputPartTwo.Add(@"<!!!>>", "0");
            program.SampleInputPartTwo.Add(@"<{o""i!a,<{i<a>", "10");

            program.Solve();
        }

        private static List<char> brackets = new List<char>() { '{', '}', '<', '>' };

        protected override string FindFirstSolution(string[] input)
        {
            string stream = input[0];
            int groupLevel = 0;
            int sumGroups = 0;
            bool isInsideGarbage = false;
            for (int i = 0; i < stream.Length; i++)
            {
                //we only care if we are working with brackets
                if (brackets.Contains(stream[i]))
                {
                    //should we ignore the bracket?
                    int pos = i - 1;
                    int numberOfIgnoreCharacters = 0;
                    while (pos >= 0 && stream[pos] == '!')
                    {
                        numberOfIgnoreCharacters++;
                        pos--;
                    }

                    if (numberOfIgnoreCharacters % 2 == 1)
                    {
                        //ignore!
                        continue;
                    }

                    //check for specific type of bracket
                    switch (stream[i])
                    {
                        case '{':
                            {
                                if (!isInsideGarbage)
                                {
                                    groupLevel++;
                                }
                                break;
                            }
                        case '}':
                            {
                                if (!isInsideGarbage)
                                {
                                    sumGroups += groupLevel--;
                                }
                                break;
                            }
                        case '<':
                            {
                                isInsideGarbage = true;
                                break;
                            }
                        case '>':
                            {
                                isInsideGarbage = false;
                                break;
                            }
                    }
                }
            }

            return sumGroups.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            string stream = input[0];
            int sumGarbageGroups = 0;
            bool isInsideGarbage = false;
            for (int i = 0; i < stream.Length; i++)
            {
                //we only care if we are working with garbage brackets
                if ((!isInsideGarbage && stream[i] == '<') || (isInsideGarbage && stream[i] == '>'))
                {
                    //should we ignore the bracket?
                    int pos = i - 1;
                    int numberOfIgnoreCharacters = 0;
                    while (pos >= 0 && stream[pos] == '!')
                    {
                        numberOfIgnoreCharacters++;
                        pos--;
                    }

                    if (numberOfIgnoreCharacters % 2 == 0)
                    {
                        //we don't ignore it!!
                        //check for specific type of bracket
                        switch (stream[i])
                        {
                            case '<':
                                {
                                    isInsideGarbage = true;
                                    break;
                                }
                            case '>':
                                {
                                    isInsideGarbage = false;
                                    break;
                                }
                        }

                        continue;
                    }
                }

                if (isInsideGarbage)
                {
                    //should we ignore the current character??
                    int pos = i - 1;
                    int numberOfIgnoreCharacters = 0;
                    while (pos >= 0 && stream[pos] == '!')
                    {
                        numberOfIgnoreCharacters++;
                        pos--;
                    }

                    if (numberOfIgnoreCharacters % 2 == 0 && stream[i] != '!')
                    {
                        sumGarbageGroups++;
                    }
                }
            }

            return sumGarbageGroups.ToString();
        }
    }
}