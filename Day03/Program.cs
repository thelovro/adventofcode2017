﻿using Common;
using System;
using System.Collections.Generic;

namespace Day03
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            var squareId = int.Parse(input[0]);
            int number = 1;
            int stepCounter = 0;
            int internalStepCounter = 0;
            Direction? dir = null;
            int x = 0;
            int y = 0;

            while (number < squareId)
            {
                if (internalStepCounter == 0)
                {
                    if (dir == null || dir == Direction.Down)
                    {
                        dir = Direction.Right;
                        stepCounter++;
                        internalStepCounter = stepCounter;
                    }
                    else if (dir == Direction.Right)
                    {
                        dir = Direction.Up;
                        internalStepCounter = stepCounter;
                    }
                    else if (dir == Direction.Up)
                    {
                        dir = Direction.Left;
                        stepCounter++;
                        internalStepCounter = stepCounter;
                    }
                    else if (dir == Direction.Left)
                    {
                        dir = Direction.Down;
                        internalStepCounter = stepCounter;
                    }
                }

                if (dir == Direction.Right) x++;
                if (dir == Direction.Up) y++;
                if (dir == Direction.Left) x--;
                if (dir == Direction.Down) y--;

                internalStepCounter--;
                number++;
            }

            return (Math.Abs(x) + Math.Abs(y)).ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            var squareId = int.Parse(input[0]);
            int number = 1;
            int stepCounter = 0;
            int internalStepCounter = 0;
            Direction? dir = null;
            int x = 0;
            int y = 0;

            Dictionary<string, int> dict = new Dictionary<string, int>();
            dict.Add(string.Format("{0}_{1}", x, y), 1);

            int result = 1;
            while (result < squareId)
            {
                if (internalStepCounter == 0)
                {
                    if (dir == null || dir == Direction.Down)
                    {
                        dir = Direction.Right;
                        stepCounter++;
                        internalStepCounter = stepCounter;
                    }
                    else if (dir == Direction.Right)
                    {
                        dir = Direction.Up;
                        internalStepCounter = stepCounter;
                    }
                    else if (dir == Direction.Up)
                    {
                        dir = Direction.Left;
                        stepCounter++;
                        internalStepCounter = stepCounter;
                    }
                    else if (dir == Direction.Left)
                    {
                        dir = Direction.Down;
                        internalStepCounter = stepCounter;
                    }
                }

                if (dir == Direction.Right) x++;
                if (dir == Direction.Up) y++;
                if (dir == Direction.Left) x--;
                if (dir == Direction.Down) y--;

                internalStepCounter--;
                number++;
                //calculate new number!!
                result = CalculateNewNumber(dict, x, y);
                dict.Add(string.Format("{0}_{1}", x, y), result);
            }

            return result.ToString();
        }

        enum Direction
        {
            Right,
            Up,
            Left,
            Down
        }

        private static int CalculateNewNumber(Dictionary<string, int> dict, int x, int y)
        {
            int sum = 0;

            sum += dict.ContainsKey(string.Format("{0}_{1}", x + 1, y)) ? dict[string.Format("{0}_{1}", x + 1, y)] : 0;
            sum += dict.ContainsKey(string.Format("{0}_{1}", x + 1, y + 1)) ? dict[string.Format("{0}_{1}", x + 1, y + 1)] : 0;
            sum += dict.ContainsKey(string.Format("{0}_{1}", x, y + 1)) ? dict[string.Format("{0}_{1}", x, y + 1)] : 0;
            sum += dict.ContainsKey(string.Format("{0}_{1}", x - 1, y + 1)) ? dict[string.Format("{0}_{1}", x - 1, y + 1)] : 0;
            sum += dict.ContainsKey(string.Format("{0}_{1}", x - 1, y)) ? dict[string.Format("{0}_{1}", x - 1, y)] : 0;
            sum += dict.ContainsKey(string.Format("{0}_{1}", x - 1, y - 1)) ? dict[string.Format("{0}_{1}", x - 1, y - 1)] : 0;
            sum += dict.ContainsKey(string.Format("{0}_{1}", x, y - 1)) ? dict[string.Format("{0}_{1}", x, y - 1)] : 0;
            sum += dict.ContainsKey(string.Format("{0}_{1}", x + 1, y - 1)) ? dict[string.Format("{0}_{1}", x + 1, y - 1)] : 0;

            return sum;
        }
    }
}