﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day21
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"../.# => ##./#../...
.#./..#/### => #..#/..../..../#..#", "12");

            program.Solve();

        }
        protected override string FindFirstSolution(string[] input)
        {
            //initial pattern
            string[,] pattern = { { ".", "#", "." }, { ".", ".", "#" }, { "#", "#", "#" } };

            Dictionary<string, string> rules = ParseRules(input);

            for (int i = 0; i < (IsTest ? 2 : 5); i++)
            {
                pattern = Enhance(pattern, rules);
            }

            var counter = pattern.Cast<string>().Where(x => x == "#").Count();

            return counter.ToString();
        }

        private string[,] Enhance(string[,] pattern, Dictionary<string, string> rules)
        {
            List<(int, int, string)> newPatternParts = new List<(int, int, string)>();
            //double noOfDividedSquares = Math.Sqrt(pattern.Length) % 2 == 0 ? Math.Sqrt(pattern.Length) / 2 : Math.Sqrt(pattern.Length) / 3;
            if (pattern.Length % 2 == 0)
            {
                //divide on 2x2 squares
                int qy = 0;
                for (int y = 0; y < Math.Sqrt(pattern.Length); y += 2)
                {
                    int qx = 0;
                    for (int x = 0; x < Math.Sqrt(pattern.Length); x += 2)
                    {
                        List<string> permutations = GetPerumationsTwoByTwo(pattern[x, y], pattern[x + 1, y], pattern[x, y + 1], pattern[x + 1, y + 1]);

                        foreach (string p in permutations)
                        {
                            if (rules.Any(r => r.Key == p))
                            {
                                newPatternParts.Add((qx, qy, rules.First(r => r.Key == p).Value));
                                continue;
                            }
                        }

                        qx++;
                    }

                    qy++;
                }
            }
            else if (pattern.Length % 3 == 0)
            {
                //divide on 3x3 squares
                
                int qy = 0;
                for (int y = 0; y < Math.Sqrt(pattern.Length); y += 3)
                {
                    int qx = 0;
                    for (int x = 0; x < Math.Sqrt(pattern.Length); x += 3)
                    {
                        List<string> permutations = GetPerumationsThreeByThree(pattern[x, y], pattern[x + 1, y], pattern[x + 2, y],
                                                                                pattern[x, y + 1], pattern[x + 1, y + 1], pattern[x + 2, y + 1],
                                                                                pattern[x, y + 2], pattern[x + 1, y + 2], pattern[x + 2, y + 2]);
                        foreach (string p in permutations)
                        {
                            if (rules.Any(r => r.Key == p))
                            {
                                newPatternParts.Add((qx, qy, rules.First(r => r.Key == p).Value));
                                continue;
                            }
                        }

                        qx++;
                    }

                    qy++;
                }
            }
            else
                throw new Exception("should not happend!!");

            //return new array
            int dividor = pattern.Length % 2 == 0 ? 2 : 3;
            int factor = pattern.Length % 2 == 0 ? 3 : 4;
            
            int dimension = (int)Math.Sqrt(pattern.Length) / dividor * factor;
            string[,] newPattern = new string[dimension, dimension];

            for (int i = 0; i < newPatternParts.Count; i++)
            {
                string[] parts = newPatternParts[i].Item3.Split("/");

                for (int fx = 0; fx < factor; fx++)
                {
                    for (int fy = 0; fy < factor; fy++)
                    {
                        newPattern[newPatternParts[i].Item1 * factor + fx, newPatternParts[i].Item2 * factor + fy] = parts[fy][fx].ToString();
                    }
                }
            }

            return newPattern;
        }

        private Dictionary<string, string> ParseRules(string[] input)
        {
            Dictionary<string, string> rules = new Dictionary<string, string>();

            foreach (string row in input)
            {
                rules.Add(row.Split(" => ")[0], row.Split(" => ")[1]);
            }

            return rules;
        }

        /// <summary>
        /// #.  12
        /// ..  34
        /// 
        /// .#  31
        /// ..  42
        /// 
        /// ..  43
        /// .#  21
        /// 
        /// ..  24
        /// #.  13
        /// </summary>
        private List<string> GetPerumationsTwoByTwo(string one, string two, string three, string four)
        {
            string init = " " + one + two + three + four;

            List<string> p = new List<string>();
            p.Add(string.Format("{0}{1}/{2}{3}", init[1], init[2], init[3], init[4]));
            p.Add(string.Format("{0}{1}/{2}{3}", init[3], init[1], init[4], init[2]));
            p.Add(string.Format("{0}{1}/{2}{3}", init[4], init[3], init[2], init[1]));
            p.Add(string.Format("{0}{1}/{2}{3}", init[2], init[4], init[1], init[3]));

            return p;
        }

        /// <summary>
        /// .#.		123
        /// ..#		456
        /// ###		789
        /// 
        /// #..		741
        /// #.#		852
        /// ##.		963
        /// 
        /// ###		987
        /// #..		654
        /// .#.		321
        /// 
        /// .##		369
        /// #.#		258
        /// ..#		147
        /// 
        /// .#.		321
        /// #..		654
        /// ###		987
        /// 
        /// ##.		963
        /// #.#		852
        /// #..		741
        /// 
        /// ###		789
        /// ..#		456
        /// .#.		123
        /// 
        /// ..#		147
        /// #.#		258
        /// .##		369
        /// </summary>
        private List<string> GetPerumationsThreeByThree(string one, string two, string three, string four, string five, string six, string seven, string eight, string nine)
        {
            string init = " " + one + two + three + four + five + six + seven + eight + nine;

            List<string> p = new List<string>();
            p.Add(string.Format("{0}{1}{2}/{3}{4}{5}/{6}{7}{8}", init[1], init[2], init[3], init[4], init[5], init[6], init[7], init[8], init[9]));
            p.Add(string.Format("{0}{1}{2}/{3}{4}{5}/{6}{7}{8}", init[7], init[4], init[1], init[8], init[5], init[2], init[9], init[6], init[3]));
            p.Add(string.Format("{0}{1}{2}/{3}{4}{5}/{6}{7}{8}", init[9], init[8], init[7], init[6], init[5], init[4], init[3], init[2], init[1]));
            p.Add(string.Format("{0}{1}{2}/{3}{4}{5}/{6}{7}{8}", init[3], init[6], init[9], init[2], init[5], init[8], init[1], init[4], init[7]));

            p.Add(string.Format("{0}{1}{2}/{3}{4}{5}/{6}{7}{8}", init[3], init[2], init[1], init[6], init[5], init[4], init[9], init[8], init[7]));
            p.Add(string.Format("{0}{1}{2}/{3}{4}{5}/{6}{7}{8}", init[9], init[6], init[3], init[8], init[5], init[2], init[7], init[4], init[1]));
            p.Add(string.Format("{0}{1}{2}/{3}{4}{5}/{6}{7}{8}", init[7], init[8], init[9], init[4], init[5], init[6], init[1], init[2], init[3]));
            p.Add(string.Format("{0}{1}{2}/{3}{4}{5}/{6}{7}{8}", init[1], init[4], init[7], init[2], init[5], init[8], init[3], init[6], init[9]));

            return p;
        }

        protected override string FindSecondSolution(string[] input)
        {
            //initial pattern
            string[,] pattern = { { ".", "#", "." }, { ".", ".", "#" }, { "#", "#", "#" } };

            Dictionary<string, string> rules = ParseRules(input);

            for (int i = 0; i < 18; i++)
            {
                pattern = Enhance(pattern, rules);
            }

            var counter = pattern.Cast<string>().Where(x => x == "#").Count();

            return counter.ToString();
        }
    }
}
