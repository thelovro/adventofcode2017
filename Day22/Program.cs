﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day22
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"..#
#..
...", "5587");

            program.SampleInputPartTwo.Add(@"..#
#..
...", "2511944");

            program.Solve();
        }

        enum Direction
        {
            Up,
            Right,
            Down,
            Left
        }

        private char Clean = '.';
        private char Infected = '#';
        private char Weakened = 'W';
        private char Flagged = 'F';

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<(int, int), char> map = MakeMap(input);

            int dimension = (input.Length / 2);
            (int, int) position = (dimension, dimension);
            Direction direction = Direction.Up;

            int counter = 0;
            for (int i = 0; i < 10000; i++)
            {
                if (map[position] == Infected)
                {
                    map[position] = Clean;
                    direction = Turn(direction, 1);
                }
                else if (map[position] == Clean)
                {
                    map[position] = Infected;
                    counter++;
                    direction = Turn(direction, -1);
                }

                position = (position.Item1 += direction == Direction.Right ? 1 : direction == Direction.Left ? -1 : 0,
                            position.Item2 += direction == Direction.Up ? -1 : direction == Direction.Down ? 1 : 0);

                if (!map.ContainsKey(position))
                    map.Add(position, Clean);
            }

            return counter.ToString();
        }

        private Dictionary<(int, int), char> MakeMap(string[] input)
        {
            Dictionary<(int, int), char> map = new();

            for (int y=0;y<input.Length;y++)
            {
                for (int x = 0; x < input[0].Length; x++)
                {
                    map.Add((x, y), input[y][x]);
                }
            }

            return map;
        }

        private Direction Turn(Direction direction, int turn)
        {
            direction += turn;
            if ((int)direction == 5)
                direction = (Direction)1;
            else if ((int)direction == 4)
                direction = (Direction)0;
            else if ((int)direction == -1)
                direction = (Direction)3;

            return direction;
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<(int, int), char> map = MakeMap(input);

            int dimension = (input.Length / 2);
            (int, int) position = (dimension, dimension);
            Direction direction = Direction.Up;

            int counter = 0;
            for (int i = 0; i < 10000000; i++)
            {
                if (map[position] == Clean)
                {
                    map[position] = Weakened;
                    direction = Turn(direction, -1);
                }
                else if (map[position] == Weakened)
                {
                    map[position] = Infected;
                    counter++;
                }
                else if (map[position] == Infected)
                {
                    map[position] = Flagged;
                    direction = Turn(direction, 1);
                }
                else if (map[position] == Flagged)
                {
                    map[position] = Clean;
                    direction = Turn(direction, 2);
                }

                position = (position.Item1 += direction == Direction.Right ? 1 : direction == Direction.Left ? -1 : 0,
                            position.Item2 += direction == Direction.Up ? -1 : direction == Direction.Down ? 1 : 0);

                if (!map.ContainsKey(position))
                    map.Add(position, Clean);
            }

            return counter.ToString();
        }
    }
}
