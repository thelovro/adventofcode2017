﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day07
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)", "tknk");

            program.SampleInputPartTwo.Add(@"pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)", "60");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Prog> progs = ParsePrograms(input);

            List<string> listOfChildren = new List<string>();
            foreach (var ch in progs.Where(x => x.Children.Count > 0).Select(x => x.Children))
            {
                listOfChildren.AddRange(ch);
            }

            string solution = progs.First(x => !listOfChildren.Contains(x.Name)).Name;

            return solution.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Prog> progs = ParsePrograms(input);

            CalculateProgramsWeight(progs);

            //determine proper weight for the program
            List<int> fixedWeights = new List<int>();
            int rightProgramWeight = 0;
            foreach (Prog program in progs.Where(x => x.Children.Count > 0))
            {
                List<int> weights = new List<int>();
                foreach (string child in program.Children)
                {
                    weights.Add(progs.First(x => x.Name == child).SumWeight);
                }

                if (weights.Distinct().Count() > 1)
                {
                    var a = program.Children.GroupBy(c => progs.First(x => x.Name == c).SumWeight).Select(g => new { g.Key, Count = g.Count() });

                    int wrongWeight = a.First(x => x.Count == 1).Key;
                    int rightWeight = a.First(x => x.Count > 1).Key;

                    var problematicProgram = program.Children.Select(c => progs.First(x => x.Name == c)).First(c => c.SumWeight == wrongWeight);
                    rightProgramWeight = problematicProgram.Weight + rightWeight - wrongWeight;

                    fixedWeights.Add(rightProgramWeight);
                }
            }

            //it will print out more solutions - only the one with the smalles weight is the right answer!
            //fixing the root of the problem also fixes the other children
            return fixedWeights.Min().ToString(); ;
        }

        private static List<Prog> ParsePrograms(string[] input)
        {
            List<Prog> progs = new List<Prog>();

            foreach (var program in input)
            {
                string programName = program.Substring(0, program.IndexOf(' '));
                int programWeight = int.Parse(program.Substring(program.IndexOf('(') + 1, program.IndexOf(')') - program.IndexOf('(') - 1));

                List<string> children = new List<string>();
                if (program.Contains(" -> "))
                {
                    int start = program.IndexOf(" -> ") + " -> ".Length;
                    children = program.Substring(start, program.Length - start).Split(", ").ToList();
                }

                progs.Add(new Prog(programName, programWeight, children));
            }

            return progs;
        }

        private static void CalculateProgramsWeight(List<Prog> progs)
        {
            foreach (Prog prog in progs)
            {
                CalculateProgramWeight(prog, progs);
            }
        }

        private static void CalculateProgramWeight(Prog prog, List<Prog> progs)
        {
            int sumWeight = prog.Weight;

            foreach (var child in prog.Children)
            {
                var childProgram = progs.First(x => x.Name == child);
                if (childProgram.SumWeight == 0)
                {
                    CalculateProgramWeight(childProgram, progs);
                }
                sumWeight += childProgram.SumWeight;
            }

            prog.SumWeight = sumWeight;
        }

        class Prog
        {
            public string Name { get; set; }
            public int Weight { get; set; }
            public List<string> Children { get; set; }
            public int SumWeight { get; set; }
            public Prog(string name, int weight, List<string> children)
            {
                Name = name;
                Weight = weight;
                Children = children;
            }
        }
    }
}