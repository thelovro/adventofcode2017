﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day18
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"set a 1
add a 2
mul a a
mod a 5
snd a
set a 0
rcv a
jgz a -1
set a 1
jgz a -2", "4");

            program.SampleInputPartTwo.Add(@"snd 1
snd 2
snd p
rcv a
rcv b
rcv c
rcv d", "3");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            var operations = ParseInput(input);

            Dictionary<string, long> registers = SetRegisters(operations);

            long frequency = FindFrequency(registers, operations);

            return frequency.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            var operations = ParseInput(input);

            Dictionary<string, long> registersProgram0 = SetRegisters(operations);
            Dictionary<string, long> registersProgram1 = SetRegisters(operations, 1);

            int numberOfSendsInProgram1 = RunBothPrograms(registersProgram0, registersProgram1, operations);

            return numberOfSendsInProgram1.ToString();
        }

        private List<Operation> ParseInput(string[] input)
        {
            List<Operation> operations = new List<Operation>();
            foreach (string row in input)
            {
                operations.Add(new Operation(row));
            }

            return operations;
        }

        private Dictionary<string, long> SetRegisters(List<Operation> operations, int p = 0)
        {
            Dictionary<string, long> registers = new Dictionary<string, long>();
            foreach (var register in operations.Select(o => o.RegisterNameX).Distinct())
            {
                int reg;
                if(int.TryParse(register, out reg))
                {
                    continue;
                }
                registers.Add(register, register == "p" ? p : 0);
            }

            return registers;
        }

        private long FindFrequency(Dictionary<string, long> registers, List<Operation> operations)
        {
            long frequency = 0;
            for (int i = 0; i < operations.Count; i++)
            {
                Operation operation = operations[i];
                switch (operation.OperationName)
                {
                    case "snd":
                        Sound(registers, operation, out frequency);
                        break;
                    case "set":
                        Set(registers, operation);
                        break;
                    case "add":
                        Add(registers, operation);
                        break;
                    case "mul":
                        Mul(registers, operation);
                        break;
                    case "mod":
                        Mod(registers, operation);
                        break;
                    case "rcv":
                        bool isRecovered = Recover(registers, operation, frequency);
                        if (isRecovered)
                        {
                            return frequency;
                        }
                        break;
                    case "jgz":
                        int jumpSize = Jgz(registers, operation);
                        if (jumpSize != 0)
                        {
                            //we have to add -1 - because outer for loop adds 1 automatically
                            i = i + jumpSize - 1;
                        }
                        break;
                }
            }

            throw new Exception("should not happend");
        }

        private int RunBothPrograms(Dictionary<string, long> registersProgram0, Dictionary<string, long> registersProgram1, List<Operation> operations)
        {
            Queue<long> queueForProgram0 = new Queue<long>();
            Queue<long> queueForProgram1 = new Queue<long>();

            int counterSendOperationsProgram1 = 0;

            int operationIdProgram0 = 0;
            int operationIdProgram1 = 0;

            bool program0waiting = false;
            bool program1waiting = false;

            while (!program0waiting || !program1waiting)
            {
                if(operationIdProgram0 < 0 || operationIdProgram0 > operations.Count-1 ||
                    operationIdProgram1 < 0 || operationIdProgram1 > operations.Count - 1)
                {
                    return counterSendOperationsProgram1;
                }

                var operationProgram0 = operations[operationIdProgram0];
                if (operationProgram0.OperationName == "rcv" && queueForProgram0.Count == 0)
                {
                    program0waiting = true;
                }
                else
                {
                    program0waiting = false;
                    int jumpProgram0;
                    RunOperation(registersProgram0, operationProgram0, queueForProgram1, queueForProgram0, out jumpProgram0);
                    operationIdProgram0 += jumpProgram0 != 0 ? jumpProgram0 : 1;
                }

                var operationProgram1 = operations[operationIdProgram1];
                if (operationProgram1.OperationName == "rcv" && queueForProgram1.Count == 0)
                {
                    program1waiting = true;
                }
                else
                {
                    program1waiting = false;
                    if (operationProgram1.OperationName == "snd")
                    {
                        counterSendOperationsProgram1++;
                    }
                    int jumpProgram1;
                    RunOperation(registersProgram1, operationProgram1, queueForProgram0, queueForProgram1, out jumpProgram1);
                    operationIdProgram1 += jumpProgram1 != 0 ? jumpProgram1 : 1;
                }
            }

            return counterSendOperationsProgram1;
        }

        private void RunOperation(Dictionary<string, long> registers, Operation operation, Queue<long> sendQueue, Queue<long> receiveQueue, out int jump)
        {
            jump = 0;

            switch (operation.OperationName)
            {
                case "snd":
                    Send(registers, operation, sendQueue);
                    break;
                case "set":
                    Set(registers, operation);
                    break;
                case "add":
                    Add(registers, operation);
                    break;
                case "mul":
                    Mul(registers, operation);
                    break;
                case "mod":
                    Mod(registers, operation);
                    break;
                case "rcv":
                    Receive(registers, operation, receiveQueue);
                    break;
                case "jgz":
                    jump = Jgz(registers, operation);
                    break;
            }
        }

        //snd X sends the value of X to the other program.
        private void Send(Dictionary<string, long> registers, Operation operation, Queue<long> queue)
        {
            int tmpValue;
            if (int.TryParse(operation.RegisterNameX, out tmpValue))
            {
                queue.Enqueue(tmpValue);
            }
            else
            {
                queue.Enqueue(registers[operation.RegisterNameX]);
            }
        }

        //rcv X receives the next value and stores it in register X
        private void Receive(Dictionary<string, long> registers, Operation operation, Queue<long> queue)
        {
            if (queue.Count > 0)
            {
                registers[operation.RegisterNameX] = queue.Dequeue();
            }
        }

        //snd X plays a sound with a frequency equal to the value of X.
        private void Sound(Dictionary<string, long> registers, Operation operation, out long frequency)
        {
            frequency = registers[operation.RegisterNameX];
        }

        //set X Y sets register X to the value of Y.
        private void Set(Dictionary<string, long> registers, Operation operation)
        {
            long value = operation.IsYRegister ? registers[operation.RegisterNameY] : operation.Y;

            registers[operation.RegisterNameX] = value;
        }

        //add X Y increases register X by the value of Y.
        private void Add(Dictionary<string, long> registers, Operation operation)
        {
            long value = operation.IsYRegister ? registers[operation.RegisterNameY] : operation.Y;

            registers[operation.RegisterNameX] += value;
        }

        //mul X Y sets register X to the result of multiplying the value contained in register X by the value of Y.
        private void Mul(Dictionary<string, long> registers, Operation operation)
        {
            long value = operation.IsYRegister ? registers[operation.RegisterNameY] : operation.Y;

            registers[operation.RegisterNameX] *= value;
        }

        //mod X Y sets register X to the remainder of dividing the value contained in register X by the value of Y (that is, it sets X to the result of X modulo Y).
        private void Mod(Dictionary<string, long> registers, Operation operation)
        {
            long value = operation.IsYRegister ? registers[operation.RegisterNameY] : operation.Y;

            registers[operation.RegisterNameX] %= value;
        }

        //rcv X recovers the frequency of the last sound played, but only when the value of X is not zero. (If it is zero, the command does nothing.)
        private bool Recover(Dictionary<string, long> registers, Operation operation, long frequency)
        {
            if (registers[operation.RegisterNameX] == 0)
            {
                return false;
            }

            return true;
        }

        //jgz X Y jumps with an offset of the value of Y, but only if the value of X is greater than zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
        private int Jgz(Dictionary<string, long> registers, Operation operation)
        {
            long checkingValue;
            int tmpValue;
            if (int.TryParse(operation.RegisterNameX, out tmpValue))
            {
                checkingValue = tmpValue;
            }
            else
            {
                checkingValue = registers[operation.RegisterNameX];
            }

            if (checkingValue <= 0)
            {
                return 0;
            }

            int value = operation.IsYRegister ? (int)registers[operation.RegisterNameY] : operation.Y;
            return value;
        }
    }

    public class Operation
    {
        public string OperationName;
        public string RegisterNameX;
        public string RegisterNameY;
        public int Y;
        public bool IsYRegister = false;

        public Operation(string row)
        {
            var items = row.Split(" ");
            OperationName = items[0];
            RegisterNameX = items[1];

            if (items.Length == 3)
            {
                if (!int.TryParse(items[2], out Y))
                {
                    RegisterNameY = items[2];
                    IsYRegister = true;
                }
            }
        }
    }
}