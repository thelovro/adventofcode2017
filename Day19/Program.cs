﻿using Common;
using System;

namespace Day19
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"     |          
     |  +--+    
     A  |  C    
 F---|----E|--+ 
     |  |  |  D 
     +B-+  +--+ 
               ", "ABCDEF");

            program.SampleInputPartTwo.Add(@"     |          
     |  +--+    
     A  |  C    
 F---|----E|--+ 
     |  |  |  D 
     +B-+  +--+ 
               ", "38");

            program.Solve();
        }

        enum Direction
        {
            Up = 0,
            Right = 1,
            Down = 2,
            Left = 3
        }

        protected override string FindFirstSolution(string[] input)
        {
            (int, int) position = (input[0].IndexOf('|'), 0);
            Direction direction = Direction.Down;
            string letter = "";

            string message = "";
            while (true)
            {
                (position, direction, letter) = MakeAMove(input, position, direction, letter);
                message += letter;

                if (input[position.Item2][position.Item1] == ' ')
                    break;
            }

            return message;
        }

        private ((int, int) position, Direction direction, string letter) MakeAMove(string[] input, (int, int) position, Direction direction, string letter)
        {
            char charOnNewPosition = input[position.Item2][position.Item1];
            letter = "";

            if (charOnNewPosition >= 65 && charOnNewPosition <= 91)
            {
                //alphabetical char
                letter = charOnNewPosition.ToString();

                if (direction == Direction.Up) position.Item2--;
                if (direction == Direction.Right) position.Item1++;
                if (direction == Direction.Down) position.Item2++;
                if (direction == Direction.Left) position.Item1--;
            }
            else if (charOnNewPosition == '+')
            {
                //we have to make a turn
                if (direction == Direction.Up || direction == Direction.Down)
                {
                    if (input[position.Item2][position.Item1 - 1] == ' ')
                    {
                        direction = Direction.Right;
                        position.Item1++;
                    }
                    else if (input[position.Item2][position.Item1 + 1] == ' ')
                    {
                        direction = Direction.Left;
                        position.Item1--;
                    }
                }
                else if (direction == Direction.Right || direction == Direction.Left)
                {
                    if (input[position.Item2 - 1][position.Item1] == ' ')
                    {
                        direction = Direction.Down;
                        position.Item2++;
                    }
                    else if (input[position.Item2 + 1][position.Item1] == ' ')
                    {
                        direction = Direction.Up;
                        position.Item2--;
                    }
                }
            }
            else
            {
                if (direction == Direction.Up) position.Item2--;
                if (direction == Direction.Right) position.Item1++;
                if (direction == Direction.Down) position.Item2++;
                if (direction == Direction.Left) position.Item1--;
            }

            return (position, direction, letter);
        }

        protected override string FindSecondSolution(string[] input)
        {
            (int, int) position = (input[0].IndexOf('|'), 0);
            Direction direction = Direction.Down;
            string letter = "";

            int counter = 0;
            while (true)
            {
                (position, direction, letter) = MakeAMove(input, position, direction, letter);
                counter++;

                if (input[position.Item2][position.Item1] == ' ')
                    break;
            }

            return counter.ToString();
        }
    }
}
