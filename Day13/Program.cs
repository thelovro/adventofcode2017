﻿using Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Day13
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"0: 3
1: 2
4: 4
6: 4", "24");

            program.SampleInputPartTwo.Add(@"0: 3
1: 2
4: 4
6: 4", "10");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<string, string> inputItems = new Dictionary<string, string>();
            foreach (var item in input)
            {
                inputItems.Add(item.Split(": ")[0], item.Split(": ")[1]);
            }

            int maxLayer = int.Parse(input[input.Length - 1].Split(": ")[0]);
            List<string> iWasCoughtIn = new List<string>();
            for (int i = 0; i < maxLayer + 1; i++)
            {
                if (inputItems.ContainsKey(i.ToString()))
                {
                    if (AmICought(i, int.Parse(inputItems[i.ToString()])))
                    {
                        iWasCoughtIn.Add(i.ToString());
                    }
                }
            }

            int sum = 0;
            foreach (string id in iWasCoughtIn)
            {
                sum += int.Parse(id) * int.Parse(inputItems[id]);
            }

            return sum.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<string, string> inputItems = new Dictionary<string, string>();
            foreach (var item in input)
            {
                inputItems.Add(item.Split(": ")[0], item.Split(": ")[1]);
            }

            int maxLayer = int.Parse(input[input.Length - 1].Split(": ")[0]);
            int delay = 0;
            while (true)
            {
                for (int i = 0; i < maxLayer + 1; i++)
                {
                    if (inputItems.ContainsKey(i.ToString()))
                    {
                        if (AmICought(i + delay, int.Parse(inputItems[i.ToString()])))
                        {
                            //increase delay, go back from the start
                            delay++;
                            i = -1;
                        }
                    }
                }

                break;
            }

            return delay.ToString();
        }

        private bool AmICought(int step, int range)
        {
            return step % ((range - 1) * 2) == 0;
        }
    }
}