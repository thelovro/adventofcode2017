﻿using Common;
using System;
using System.Linq;

namespace Day05
{
    class Program : BaseAoc
    {
        const string sampleInput = @"";

        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"0
3
0
1
-3", "5");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int[] array = input.Select(a => int.Parse(a)).ToArray();
            int counter = 0;
            int position = 0;
            
            while (position >= 0 && position < array.Length)
            {
                position = position + array[position]++;
                counter++;
            }

            return counter.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            int[] array = input.Select(a => int.Parse(a)).ToArray();
            int counter = 0;
            int position = 0;
            int newPosition = 0;

            while (position >= 0 && position < array.Length)
            {
                newPosition = position + array[position];
                if (array[position] >= 3)
                    array[position]--;
                else
                {
                    array[position]++;
                }
                position = newPosition;
                counter++;
            }

            return counter.ToString();
        }
    }
}