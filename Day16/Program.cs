﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day16
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"s1,x3/4,pe/b", "baedc");

            program.SampleInputPartTwo.Add(@"s1,x3/4,pe/b", "abcde");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            char[] list = (IsTest ? "abcde" : "abcdefghijklmnop").ToArray();
            list = Dance(input[0].Split(","), list);

            return string.Join("", list);
        }

        private static char[] Dance(string[] moves, char[] list)
        {
            foreach (var move in moves)
            {
                switch (move[0])
                {
                    case 's':
                        {
                            int steps = int.Parse(move.TrimStart('s'));
                            char[] listTmp = new char[list.Length];
                            for (int i = 0; i < list.Length; i++)
                            {
                                int newPostition = i + steps;
                                while (newPostition >= list.Length)
                                {
                                    newPostition -= list.Length;
                                }

                                listTmp[newPostition] = list[i];
                            }
                            list = listTmp;
                            break;
                        }
                    case 'x':
                        {
                            string[] positions = move.TrimStart('x').Split('/');
                            var tmpValue = list[int.Parse(positions[0])];
                            list[int.Parse(positions[0])] = list[int.Parse(positions[1])];
                            list[int.Parse(positions[1])] = tmpValue;

                            break;
                        }
                    case 'p':
                        {
                            char[] items = move.Substring(1).Replace("/", "").ToArray();

                            int indexOne = Array.IndexOf(list, items[0]);
                            int indexTwo = Array.IndexOf(list, items[1]);

                            var tmpValue = list[indexOne];
                            list[indexOne] = list[indexTwo];
                            list[indexTwo] = tmpValue;

                            break;
                        }
                }
            }

            return list;
        }

        protected override string FindSecondSolution(string[] input)
        {
            char[] list = (IsTest ? "abcde" : "abcdefghijklmnop").ToArray();
            string[] moves = input[0].Split(",");
            List<string> knownPositions = new List<string>();
            int repeatNumberOfSteps = -1;
            for (int z = 0; z < 1000000000; z++)
            {
                list = Dance(moves, list);

                if (knownPositions.Contains(string.Join("", list)))
                {
                    repeatNumberOfSteps = z;
                    break;
                }
                knownPositions.Add(string.Join("", list));
            }

            int numberOfStepsNeeded = 1000000000 % repeatNumberOfSteps;

            //reset the data
            list = (IsTest ? "abcde" : "abcdefghijklmnop").ToArray();
            for (int z = 0; z < numberOfStepsNeeded; z++)
            {
                list = Dance(moves, list);
            }

            return string.Join("", list);
        }
    }
}