﻿using Common;
using System;
using System.Collections.Generic;

namespace Day02
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"5	1	9	5
7	5	3	
2	4	6	8", "18");

            program.SampleInputPartTwo.Add(@"5	9	2	8
9	4	7	3
3	8	6	5", "9");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int sum = 0;
            foreach (var row in input)
            {
                var values = row.Split('\t', StringSplitOptions.RemoveEmptyEntries);
                int max = 0;
                int min = 9999;
                foreach (var value in values)
                {
                    int v = int.Parse(value);
                    if (v > max)
                    {
                        max = v;
                    }
                    if (v < min)
                    {
                        min = v;
                    }
                }

                sum += max - min;
            }

            return sum.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            int sum = 0;
            foreach (var row in input)
            {
                var values = row.Split('\t', StringSplitOptions.RemoveEmptyEntries);
                foreach (var value in values)
                {
                    int v = int.Parse(value);

                    foreach (var valueInner in values)
                    {
                        int vInner = int.Parse(valueInner);
                        if (v == vInner)
                        {
                            continue;
                        }

                        if (v % vInner == 0)
                        {
                            sum += v / vInner;
                        }
                    }
                }
            }

            return sum.ToString();
        }
    }
}