﻿using Common;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Day12
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"0 <-> 2
1 <-> 1
2 <-> 0, 3, 4
3 <-> 2, 4
4 <-> 2, 3, 6
5 <-> 6
6 <-> 4, 5", "6");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<string, List<string>> items = ParseInput(input);

            List<string> programs = new List<string>();
            FindAllConnectedPrograms("0", programs, items);

            return programs.Count().ToString();
        }

        private static Dictionary<string, List<string>> ParseInput(string[] input)
        {
            Dictionary<string, List<string>> items = new Dictionary<string, List<string>>();
            foreach (string line in input)
            {
                string[] parts = line.Split(" <-> ");
                items.Add(parts[0], new List<string>(parts[1].Split(", ").AsEnumerable()));
            }

            return items;
        }

        private List<string> FindAllConnectedPrograms(string programId, List<string> programs, Dictionary<string, List<string>> items)
        {
            if (programs.Contains(programId))
            {
                return programs;
            }

            programs.Add(programId);

            foreach (string item in items[programId])
            {
                FindAllConnectedPrograms(item, programs, items);
            }

            return programs;
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<string, List<string>> items = ParseInput(input);

            IEnumerable<string> allPrograms = items.GroupBy(x => x.Key).Select(x => x.Key).Distinct();

            int groupCount = 0;
            List<string> programs = new List<string>();
            FindAllConnectedPrograms("0", programs, items);
            groupCount++;

            while (allPrograms.Where(x => !programs.Contains(x)).Any())
            {
                FindAllConnectedPrograms(allPrograms.Where(x => !programs.Contains(x)).First(), programs, items);
                groupCount++;
            }

            return groupCount.ToString();
        }
    }
}