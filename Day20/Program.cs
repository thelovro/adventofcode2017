﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day20
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"p=<3,0,0>, v=<2,0,0>, a=<-1,0,0>
p=<4,0,0>, v=<0,0,0>, a=<-2,0,0>", "0");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Particle> particles = ParseParticles(input);

            for(int i = 0; i< 1000; i++)
            {
                foreach (Particle p in particles)
                {
                    p.VelocitiyX += p.AccelerationX;
                    p.VelocitiyY += p.AccelerationY;
                    p.VelocitiyZ += p.AccelerationZ;

                    p.X += p.VelocitiyX;
                    p.Y += p.VelocitiyY;
                    p.Z += p.VelocitiyZ;
                }
            }

            return particles.First(p => p.Manhattan == particles.Min(x => x.Manhattan)).ID.ToString();
        }

        private List<Particle> ParseParticles(string[] input)
        {
            List<Particle> particles = new List<Particle>();

            for(int i=0;i<input.Length;i++)
            {
                Particle p = new Particle();
                //p =< 3,0,0 >, v =< 2,0,0 >, a =< -1,0,0 >
                string[] parts = input[i].Split(", ");
                string[] positon = parts[0].Replace("p=<", "").Replace(">", "").Split(',');
                string[] velocity= parts[1].Replace("v=<", "").Replace(">", "").Split(',');
                string[] acceleration = parts[2].Replace("a=<", "").Replace(">", "").Split(',');

                p.X = int.Parse(positon[0]);
                p.Y = int.Parse(positon[1]);
                p.Z = int.Parse(positon[2]);

                p.VelocitiyX = int.Parse(velocity[0]);
                p.VelocitiyY = int.Parse(velocity[1]);
                p.VelocitiyZ = int.Parse(velocity[2]);

                p.AccelerationX = int.Parse(acceleration[0]);
                p.AccelerationY = int.Parse(acceleration[1]);
                p.AccelerationZ = int.Parse(acceleration[2]);

                p.IsCollided = false;
                p.ID = i;

                particles.Add(p);
            }

            return particles;
        }

        private class Particle
        {
            public long ID { get; set; }

            public long X { get; set; }
            public long Y { get; set; }
            public long Z { get; set; }
            
            public long VelocitiyX { get; set; }
            public long VelocitiyY { get; set; }
            public long VelocitiyZ { get; set; }
            
            public long AccelerationX { get; set; }
            public long AccelerationY { get; set; }
            public long AccelerationZ { get; set; }

            public long Manhattan
            {
                get
                {
                    return Math.Abs(X) + Math.Abs(Y) + Math.Abs(Z);
                }
            }

            public bool IsCollided { get; set; }
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Particle> particles = ParseParticles(input);

            for (int i = 0; i < 200; i++)
            {
                foreach (Particle p in particles)
                {
                    if (p.IsCollided)
                        continue;

                    p.VelocitiyX += p.AccelerationX;
                    p.VelocitiyY += p.AccelerationY;
                    p.VelocitiyZ += p.AccelerationZ;

                    p.X += p.VelocitiyX;
                    p.Y += p.VelocitiyY;
                    p.Z += p.VelocitiyZ;
                }

                particles = RemoveCollidedParticles(particles);
            }

            return particles.Count(p => !p.IsCollided).ToString();
        }

        private List<Particle> RemoveCollidedParticles(List<Particle> particles)
        {
            var collidedPositions = particles.GroupBy(p => new { p.X, p.Y, p.Z }).Where(p => p.Count() > 1);

            foreach(var pos in collidedPositions)
            {
                foreach(var p in particles.Where(p =>p.X == pos.Key.X && p.Y == pos.Key.Y && p.Z == pos.Key.Z))
                {
                    p.IsCollided = true;
                }
            }

            return particles;
        }
    }
}
