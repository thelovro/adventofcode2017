﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day25
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"Begin in state A.
Perform a diagnostic checksum after 6 steps.

In state A:
  If the current value is 0:
    - Write the value 1.
    - Move one slot to the right.
    - Continue with state B.
  If the current value is 1:
    - Write the value 0.
    - Move one slot to the left.
    - Continue with state B.

In state B:
  If the current value is 0:
    - Write the value 1.
    - Move one slot to the left.
    - Continue with state A.
  If the current value is 1:
    - Write the value 1.
    - Move one slot to the right.
    - Continue with state A.", "3");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            string state = GetValueWithRegex("Begin in state ", input[0]);
            long checksum = Convert.ToInt64(GetValueWithRegex("Perform a diagnostic checksum after ", input[1]));

            Dictionary<string, State> states = ParseStates(input.Skip(3).ToList());

            LinkedList<long> tape = new();
            tape.AddFirst(0);
            var current = tape.First;

            int counter = 0;
            while(counter < checksum)
            {
                if (current.Next == null)
                {
                    tape.AddAfter(current, 0);
                }
                if (current.Previous == null)
                {
                    tape.AddBefore(current, 0);
                }

                if(current.Value == 0)
                {
                    current.Value = states[state].valueIfZero;
                    current = states[state].moveRightIfZero ? current.Next : current.Previous;
                    state = states[state].nextStateIfZero;
                }
                else
                {
                    current.Value = states[state].valueIfNotZero;
                    current = states[state].moveRightIfNotZero ? current.Next : current.Previous;
                    state = states[state].nextStateIfNotZero;
                }

                counter++;
            }

            return tape.Sum().ToString();
        }

        private Dictionary<string, State> ParseStates(List<string> input)
        {
            Dictionary<string, State> states = new();
            for (int i = 0; i < input.Count(); i+=10)
            {
                states.Add(GetValueWithRegex("In state ", input[i]),
                    new State(GetValueWithRegex("In state ", input[i]),
                    Convert.ToInt32(GetValueWithRegex("    - Write the value ", input[i + 2])),
                    GetValueWithRegex("    - Move one slot to the ", input[i + 3]) == "right",
                    GetValueWithRegex("    - Continue with state ", input[i + 4]),
                    Convert.ToInt32(GetValueWithRegex("    - Write the value ", input[i + 6])),
                    GetValueWithRegex("    - Move one slot to the ", input[i + 7]) == "right",
                    GetValueWithRegex("    - Continue with state ", input[i + 8])));
            }

            return states;
        }

        private string GetValueWithRegex(string regex, string text)
        {
            var r = new Regex($"{regex}(?<g>[a-zA-Z0-9]+).+");

            return r.Match(text).Groups["g"].Value;
        }

        public record State(string name, int valueIfZero, bool moveRightIfZero, string nextStateIfZero, int valueIfNotZero, bool moveRightIfNotZero, string nextStateIfNotZero);

        protected override string FindSecondSolution(string[] input)
        {
            return "";
        }
    }
}
