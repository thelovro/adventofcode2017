﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day08
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.SampleInputPartOne.Add(@"b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10", "1");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Operation> operations = ParseInput(input);
            (Dictionary<string, int> registers, int maxValueDuringProcess) = SetRegisters(operations);

            var maxRegisterValue = registers.Values.Max();
            return maxRegisterValue.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Operation> operations = ParseInput(input);
            (Dictionary<string, int> registers, int maxValueDuringProcess) = SetRegisters(operations);

            return maxValueDuringProcess.ToString();
        }

        private (Dictionary<string, int>, int) SetRegisters(List<Operation> operations)
        {
            Dictionary<string, int> registers = new Dictionary<string, int>();
            int maxValue = 0;
            foreach (var operation in operations)
            {
                if (!registers.ContainsKey(operation.Condition.RegisterName))
                {
                    registers.Add(operation.Condition.RegisterName, 0);
                }

                if (CheckCondition(registers[operation.Condition.RegisterName], operation.Condition.Operation, operation.Condition.Amount))
                {
                    if (!registers.ContainsKey(operation.RegisterName))
                    {
                        registers.Add(operation.RegisterName, 0);
                    }

                    if (operation.OperationType == Operation.OperationTypeEnum.Decrease)
                    {
                        registers[operation.RegisterName] -= operation.Amount;
                    }
                    else
                    {
                        registers[operation.RegisterName] += operation.Amount;
                    }

                    if (registers[operation.RegisterName] > maxValue)
                    {
                        maxValue = registers[operation.RegisterName];
                    }
                }
            }

            return (registers, maxValue);
        }

        private static bool CheckCondition(int currentAmountInRegister, string operation, int conditionAmount)
        {
            switch (operation)
            {
                case "<":
                    return currentAmountInRegister < conditionAmount;
                case "<=":
                    return currentAmountInRegister <= conditionAmount;
                case ">":
                    return currentAmountInRegister > conditionAmount;
                case ">=":
                    return currentAmountInRegister >= conditionAmount;
                case "==":
                    return currentAmountInRegister == conditionAmount;
                case "!=":
                    return currentAmountInRegister != conditionAmount;
            }

            throw new Exception("should not happend");
        }

        private static List<Operation> ParseInput(string[] input)
        {
            List<Operation> operations = new List<Operation>();

            foreach (var operation in input)
            {
                string[] operationItem = operation.Split(' ');
                operations.Add(new Operation()
                {
                    RegisterName = operationItem[0],
                    OperationType = (operationItem[1] == "inc" ? Operation.OperationTypeEnum.Increase : Operation.OperationTypeEnum.Decrease),
                    Amount = int.Parse(operationItem[2]),
                    Condition = new Condition()
                    {
                        RegisterName = operationItem[4],
                        Operation = operationItem[5],
                        Amount = int.Parse(operationItem[6])
                    }
                });
            }

            return operations;
        }
    }

    public class Operation
    {
        public string RegisterName { get; set; }
        public enum OperationTypeEnum
        {
            Increase,
            Decrease
        }
        public OperationTypeEnum OperationType { get; set; }

        public int Amount { get; set; }
        public Condition Condition { get; set; }
    }

    public class Condition
    {
        public string RegisterName { get; set; }
        public string Operation { get; set; }
        public int Amount { get; set; }
    }
}