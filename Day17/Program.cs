﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day17
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"3", "638");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int steps = int.Parse(input[0]);
            int limit = 2017;
            (LinkedList<int> list, LinkedListNode<int> node) = PopulateList(steps, limit);

            return node.Next.Value.ToString();
        }

        private static (LinkedList<int>, LinkedListNode<int>) PopulateList(int steps, int limit)
        {
            LinkedList<int> list = new LinkedList<int>();
            LinkedListNode<int> currentNode = list.AddFirst(0);
            for (int i = 1; i <= limit; i++)
            {
                for (int s = 0; s < steps; s++)
                {
                    if (currentNode == currentNode.List.Last)
                    {
                        currentNode = currentNode.List.First;
                    }
                    else
                    {
                        currentNode = currentNode.Next;
                    }
                }

                currentNode = list.AddAfter(currentNode, i);
            }

            return (list, currentNode);
        }

        protected override string FindSecondSolution(string[] input)
        {
            int steps = int.Parse(input[0]);
            int limit = 50000000;
            (LinkedList<int> list, LinkedListNode<int> node) = PopulateList(steps, limit);

            //quite a large number. It takes about 10 minutes to get the solution! (39051595)
            return list.First.Next.Value.ToString();
        }
    }
}